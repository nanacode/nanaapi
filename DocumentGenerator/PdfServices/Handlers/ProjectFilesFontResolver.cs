﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using PdfSharpCore.Drawing;
using PdfSharpCore.Fonts;
using PdfSharpCore.Internal;
using PdfSharpCore.Utils;
using SixLabors.Fonts;

namespace DocumentGenerator
{
    /// <summary>
    /// Standard MigraDoc resolver (with changed constructor for custom fonts handling)
    /// </summary>
    internal class ProjectFilesFontResolver : IFontResolver
    {
        private static readonly Dictionary<string, FontFamilyModel> InstalledFonts = new Dictionary<string, FontFamilyModel>();
        private static readonly string[] SSupportedFonts;

        public string DefaultFontName
        {
            get
            {
                return "Arial";
            }
        }

        static ProjectFilesFontResolver()
        {
            var appDomain = System.AppDomain.CurrentDomain;
            var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
            var fontsPath = Path.Combine(basePath, "Fonts");

            SSupportedFonts = Directory.GetFiles(fontsPath, "*.ttf", SearchOption.AllDirectories);
            SetupFontsFiles(SSupportedFonts);
        }

        public static void SetupFontsFiles(string[] sSupportedFonts)
        {
            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
            foreach (string sSupportedFont in sSupportedFonts)
            {
                try
                {
                    string key = FontDescription.LoadDescription(sSupportedFont).FontFamily(CultureInfo.InvariantCulture);
                    Console.WriteLine(sSupportedFont);
                    List<string> stringList;
                    if (dictionary.TryGetValue(key, out stringList))
                        stringList.Add(sSupportedFont);
                    else
                        dictionary.Add(key, new List<string>()
            {
              sSupportedFont
            });
                }
                catch (Exception ex)
                {
                    Console.WriteLine((object)ex);
                }
            }
            foreach (KeyValuePair<string, List<string>> fontFamily in dictionary)
            {
                try
                {
                    InstalledFonts.Add(fontFamily.Key.ToLower(), DeserializeFontFamily(fontFamily));
                }
                catch (Exception ex)
                {
                    Console.WriteLine((object)ex);
                }
            }
        }

        private static FontFamilyModel DeserializeFontFamily(
          KeyValuePair<string, List<string>> fontFamily)
        {
            FontFamilyModel fontFamilyModel = new FontFamilyModel()
            {
                Name = fontFamily.Key
            };
            List<string> fontList = fontFamily.Value;
            if (fontFamily.Value.Count == 1)
            {
                fontFamilyModel.FontFiles.Add(XFontStyle.Regular, fontFamily.Value[0]);
                return fontFamilyModel;
            }
            if (fontList.Any<string>((Func<string, bool>)(e => e.Length != fontList[0].Length)))
            {
                IOrderedEnumerable<string> source = fontList.OrderBy<string, int>((Func<string, int>)(o => o.Length));
                fontFamilyModel.FontFiles.Add(XFontStyle.Regular, source.First<string>());
                foreach (string fontFileName in source.Skip<string>(1))
                {
                    KeyValuePair<XFontStyle, string> keyValuePair = DeserializeFontName(fontFileName);
                    if (!fontFamilyModel.FontFiles.ContainsKey(keyValuePair.Key))
                        fontFamilyModel.FontFiles.Add(keyValuePair.Key, keyValuePair.Value);
                }
                return fontFamilyModel;
            }
            foreach (string fontFileName in fontList)
            {
                KeyValuePair<XFontStyle, string> keyValuePair = DeserializeFontName(fontFileName);
                if (!fontFamilyModel.FontFiles.ContainsKey(keyValuePair.Key))
                    fontFamilyModel.FontFiles.Add(keyValuePair.Key, keyValuePair.Value);
            }
            return fontFamilyModel;
        }

        private static KeyValuePair<XFontStyle, string> DeserializeFontName(
          string fontFileName)
        {
            string withoutExtension = Path.GetFileNameWithoutExtension(fontFileName);
            string str1;
            if (withoutExtension == null)
                str1 = (string)null;
            else
                str1 = withoutExtension.ToLower().TrimEnd('-', '_');
            string str2 = str1;
            if (str2 == null)
                return new KeyValuePair<XFontStyle, string>(XFontStyle.Regular, (string)null);
            if (str2.Contains("bold") && str2.Contains("italic") || (str2.EndsWith("bi", StringComparison.Ordinal) || str2.EndsWith("ib", StringComparison.Ordinal)) || str2.EndsWith("z", StringComparison.Ordinal))
                return new KeyValuePair<XFontStyle, string>(XFontStyle.BoldItalic, fontFileName);
            if (str2.Contains("bold") || str2.EndsWith("b", StringComparison.Ordinal) || str2.EndsWith("bd", StringComparison.Ordinal))
                return new KeyValuePair<XFontStyle, string>(XFontStyle.Bold, fontFileName);
            return str2.Contains("italic") || str2.EndsWith("i", StringComparison.Ordinal) || str2.EndsWith("ib", StringComparison.Ordinal) ? new KeyValuePair<XFontStyle, string>(XFontStyle.Italic, fontFileName) : new KeyValuePair<XFontStyle, string>(XFontStyle.Regular, fontFileName);
        }

        public byte[] GetFont(string faceFileName)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                string path = "";
                try
                {
                    path = ((IEnumerable<string>)SSupportedFonts).ToList<string>().First<string>((Func<string, bool>)(x => x.ToLower().Contains(Path.GetFileName(faceFileName).ToLower())));
                    using (FileStream fileStream = File.OpenRead(path))
                    {
                        fileStream.CopyTo((Stream)memoryStream);
                        memoryStream.Position = 0L;
                        return memoryStream.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine((object)ex);
                    throw new Exception("No Font File Found - " + faceFileName + " - " + path);
                }
            }
        }

        public FontResolverInfo ResolveTypeface(
          string familyName,
          bool isBold,
          bool isItalic)
        {
            if (InstalledFonts.Count == 0)
                throw new FileNotFoundException("No Fonts installed on this device!");
            KeyValuePair<XFontStyle, string> keyValuePair = InstalledFonts.First<KeyValuePair<string, FontFamilyModel>>().Value.FontFiles.First<KeyValuePair<XFontStyle, string>>();
            string path = keyValuePair.Value;
            FontFamilyModel fontFamilyModel;
            if (!InstalledFonts.TryGetValue(familyName.ToLower(), out fontFamilyModel))
                return new FontResolverInfo(Path.GetFileName(path));
            if (isBold & isItalic)
            {
                if (fontFamilyModel.FontFiles.TryGetValue(XFontStyle.BoldItalic, out path))
                    return new FontResolverInfo(Path.GetFileName(path));
            }
            else if (isBold)
            {
                if (fontFamilyModel.FontFiles.TryGetValue(XFontStyle.Bold, out path))
                    return new FontResolverInfo(Path.GetFileName(path));
            }
            else if (isItalic && fontFamilyModel.FontFiles.TryGetValue(XFontStyle.Italic, out path))
                return new FontResolverInfo(Path.GetFileName(path));
            if (fontFamilyModel.FontFiles.TryGetValue(XFontStyle.Regular, out path))
                return new FontResolverInfo(Path.GetFileName(path));
            keyValuePair = fontFamilyModel.FontFiles.First<KeyValuePair<XFontStyle, string>>();
            return new FontResolverInfo(Path.GetFileName(keyValuePair.Value));
        }
    }
}