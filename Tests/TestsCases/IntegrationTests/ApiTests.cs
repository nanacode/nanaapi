﻿using DatabaseAccess;
using Microsoft.AspNetCore.Mvc.Testing;
using NanaApi;
using System;
using Microsoft.Extensions.DependencyInjection;
using Tests.Utilities;
using Xunit;
using System.Linq;

namespace Tests.TestCases
{
    [Collection("Sequential"), TestCaseOrderer("Tests.Utilities.AlphabeticalOrderer", "Tests")]
    public class ApiTests : BaseForTests, IDisposable
    {
        public ApiTests(WebApplicationFactoryWithInMemorySqlite<Startup> factory) : base(factory)
        {
        }

        public void Dispose()
        {
        }

       
        [Fact]
        public void Chapter1_Ping()
        {
            //ARRANGE
            //

            //ACT
            var response = NanaClient.PingDb();

            //ASSERT
            Assert.Equal("Sam", response);
        }

        [Fact]
        public void Chapter2_Ping2()
        {
            //ARRANGE
            var count = 0;

            //ACT
           
            _factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var serviceProvider = services.BuildServiceProvider();
                    using (var scope = serviceProvider.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        var context = scopedServices.GetRequiredService<hobbitsContext>();
                        count = context.Hobbits.Count();
                    }
                });
            }).CreateClient(new WebApplicationFactoryClientOptions { AllowAutoRedirect = false });


            //ASSERT
            Assert.Equal(1, count);
        }

    }
}
