﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanaApi.WebModels
{
    public class BigInfoBox
    {
        public BigInfoBoxVersion Version { get; set; }
        public string H2Title { get; set; }
        public string H3Title { get; set; }
        public string Paragraph { get; set; }
        public List<string> Alerts { get; set; } 
    }

    public enum BigInfoBoxVersion
    {
        yes,
        no,
        error
    }
}
