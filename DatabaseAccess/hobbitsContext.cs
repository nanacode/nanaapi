﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DatabaseAccess
{
    public partial class hobbitsContext : DbContext
    {
        public hobbitsContext()
        {
        }

        public hobbitsContext(DbContextOptions<hobbitsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Hobbits> Hobbits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hobbits>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
