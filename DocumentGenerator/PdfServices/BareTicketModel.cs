﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentGenerator.PdfServices
{
    public class BareTicketModel
    {
        public byte[] Logo { get; set; }
        public string TicketTypeName { get; set; }
        public byte[] QrImage { get; set; }
        public string QrText { get; set; }
        public string ProcessTypeName { get; set; }
    }
}
