﻿using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography
{
    /// <summary>
    /// Service handling all cryptography based operations
    /// </summary>
    public class CryptographyService : ICryptographyService
    {
        public string Decrypt(string encryptedText, string key128)
        {
            return DecryptStringAES(encryptedText, key128);
        }

        public string Encrypt(string data, string key128)
        {
            return EncryptStringAES(data, key128);
        }

        /// <summary>
        /// Generates date based guid
        /// </summary>
        /// <returns>Guid with pattern YYYYMMdd-$$$$$, where dollars are random uppercase characters or digits</returns>
        public string GenerateGuid()
        {
            var rngcode = GetRandomCharacterSequence(5, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
            var date = DateTime.Now;

            return $"{date.Year}{date:MM}{date:dd}-{rngcode}";
        }

        /// <summary>
        /// Generates 128 bit key with salt. Bases on salting byte by byte and MD5 128 bit hash.
        /// </summary>
        /// <param name="key">Key to be "salted"</param>
        /// <param name="salt">Salt</param>
        /// <returns>Ready to go salted 128 bit key</returns>
        public string Get128KeyWithSalt(string key, string salt)
        {
            var plainText = Encoding.UTF8.GetBytes(key);
            var saltBytes = Encoding.UTF8.GetBytes(salt);

            byte[] plainTextWithSaltBytes = new byte[plainText.Length + salt.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = saltBytes[i];
            }

            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(plainTextWithSaltBytes);

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }


        /// <summary>
        /// Creates random sequence of given characters with specified length using cryptographic RNG
        /// </summary>
        /// <param name="length">Length of output string</param>
        /// <param name="availableCharacters">Available characters for sequence generation</param>
        /// <returns>Randomized sequence of characters</returns>
        public string GetRandomCharacterSequence(int length, string availableCharacters)
        {
            char[] chars = new char[availableCharacters.Length];
            chars = availableCharacters.ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[length];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(length);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        private string EncryptStringAES(string plainText, string key128)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key128);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return WebEncoders.Base64UrlEncode(array);
        }

        private string DecryptStringAES(string cipherText, string key128)
        {
            byte[] iv = new byte[16];
            byte[] buffer = WebEncoders.Base64UrlDecode(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key128);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

    }
}
