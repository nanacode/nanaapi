using DatabaseAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace NanaApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// #ADDS STATIC FILES
        /// </summary>
        public void AddStaticFiles(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(env.ContentRootPath, "ViewsAssets")),
                RequestPath = "/ViewsAssets"
            });
        }

        /// <summary>
        /// #DETECTS PROPER IP ADDRESSES
        /// </summary>
        public void ConfigureIpAddresses(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }

        /// <summary>
        /// #ALLOWS TO RETURN OWN 400 RESPONSE
        /// </summary>
        public void TurnOffAuto400(IServiceCollection services)
        {
            services.Configure<Microsoft.AspNetCore.Mvc.ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        /// <summary>
        /// #FORCE USING NEWTONSOFT DURING API CONVERSIONS
        /// </summary>
        public void ForceNewtonsoft(IServiceCollection services)
        {
            services.AddControllersWithViews().AddNewtonsoftJson();
        }

        /// <summary>
        /// #INJECT DBCONTEXT
        /// </summary>
        public void InjectDbContext(IServiceCollection services)
        {
            var x = Configuration.GetConnectionString("nanaConnectionString");
            //#Connection string
            services.AddDbContext<hobbitsContext>(options => options.UseSqlite(Configuration.GetConnectionString("nanaConnectionString")));
        }

        /// <summary>
        /// #INJECT SERVICES
        /// </summary>
        public void InjectServices(IServiceCollection services)
        {
            //#Standard DI
            //services.AddScoped<IService, Class>();
            //services.AddTransient<IService>(x => new Class(
            //    Configuration.GetSection("Section")["field1"],
            //    Configuration.GetSection("Section")["field2"],
            //    Configuration.GetSection("Section")["field3"]
            //    ));


            //#Differ DI
            if (Configuration.GetValue<string>("EnvSettingsName") == "Development")
            {
                //services.AddScoped<IService, Class>();
            }
            else
            {
                //services.AddScoped<IService, Class>();
            }
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            TurnOffAuto400(services);
            InjectServices(services);
            InjectDbContext(services);
            ForceNewtonsoft(services);
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _webHostEnvironment = env;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            //cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            AddStaticFiles(app, env);
            ConfigureIpAddresses(app, env);
        }
    }
}
