﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseAccess
{

    public class HobbitsAccess
    {
        private readonly hobbitsContext _context;

        public HobbitsAccess(hobbitsContext context)
        {
            _context = context;
        }

        public string GetFirstHobbit()
        {
            return _context.Hobbits.First().Name;
        }
    }
}
