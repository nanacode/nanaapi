﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Mail
{
    /// <summary>
    /// Based on SmtpClient simple mailer
    /// </summary>
    public class MailService : IMailService
    {
        private readonly ISmtpClientProvider _smtpClientProvider;


        public MailService(ISmtpClientProvider smtpClientProvider)
        {
            _smtpClientProvider = smtpClientProvider;
        }


        public MailMessage SendInternalMail(string addressEmailFrom, string addressEmailTo, string title, string body, List<Attachment> attachments)
        {
            var mail = new MailMessage();

            try
            {
                mail.To.Add(addressEmailTo);
                mail.From = new MailAddress(addressEmailFrom);
                mail.Subject = SanitizeSubject(title);
                mail.Body = body;
                foreach (var attachment in attachments)
                    mail.Attachments.Add(attachment);

                mail.IsBodyHtml = true;

                _smtpClientProvider.GetSmtpClient().Send(mail);
                return mail;
            }
            catch (Exception e)
            {
                throw new MailException(mail, _smtpClientProvider, "Error during sending Email", e);
            }
        }

        private string SanitizeSubject(string toSanitize)
        {
            var res = toSanitize;
            try
            {
                string replaceWith = "";
                return res.Replace("\r\n", replaceWith).Replace("\n", replaceWith).Replace("\r", replaceWith);
            }
            catch
            {
                return toSanitize;
            }
        }

    }
}
