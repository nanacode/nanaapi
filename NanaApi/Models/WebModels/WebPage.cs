﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanaApi.WebModels
{
    public class WebPage
    {
        public WebPage()
        {
            RandBg();
        }
        public string SeoTitle { get; set; }
        public string BgClass { get; set; }

        private void RandBg()
        {
            Random rnd = new Random();
            int num = rnd.Next(1, 4);
            BgClass = "bg" + num.ToString();
        }
    }
}
