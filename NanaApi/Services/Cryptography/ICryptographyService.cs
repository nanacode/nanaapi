﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cryptography
{
    public interface ICryptographyService
    {
        public string GenerateGuid();
        public string Get128KeyWithSalt(string key, string salt);
        public string Encrypt(string data, string key);
        public string Decrypt(string encryptedText, string key);
        public string GetRandomCharacterSequence(int length, string availableCharacters);
    }
}
