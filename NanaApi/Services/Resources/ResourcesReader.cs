﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Resources
{
    public class ResourcesReader
    {
        public string GetSomeText()
        {
            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"\Services\Resources" + @"\somepage.html";
            var fileString = File.ReadAllText(filePath);
            return fileString;
        }

        public byte[] GetSomePng()
        {
            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"\Services\Resources" + @"\test.png";
            var bytes = File.ReadAllBytes(filePath);
            return bytes;
        }
    }
}
