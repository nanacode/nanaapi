﻿using System.IO;
using MigraDocCore.DocumentObjectModel;
using MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes;
using MigraDocCore.Rendering;
using PdfSharpCore.Fonts;
using PdfSharpCore.Utils;
using SixLabors.ImageSharp.PixelFormats;
using MigraDoc.DocumentObjectModel.Shapes;
using DocumentGenerator.PdfServices.Handlers;
using DocumentGenerator.PdfServices;

namespace DocumentGenerator
{
    /// <summary>
    /// Service based on .NET Core MigraDoc for background ticket generation
    /// </summary>
    public class TicketService
    {
        /// <summary>
        /// Generates ticket pdf file
        /// </summary>
        /// <param name="model">Model with ticket data</param>
        /// <returns>Bytes array of pdf file</returns>
        public byte[] GetPdf(TicketModel model)
        {
            //Get manager to control easily Document object
            var migraManager = new MigraDocManager();

            //Prepare document
            var document = migraManager.PrepareDocument(model.QrText);

            //Define Styles
            DefineStyles(document, model);

            //Draw something fancy:
            //-------
            Section section = document.AddSection();

            //Add page background
            var myImage = ImageSource.FromBinary("bg", () => model.BackgroundImage);
            var image = section.AddImage(myImage);
            image.Width = "21cm";
            image.Height = "29.7cm";
            image.RelativeVertical = (MigraDocCore.DocumentObjectModel.Shapes.RelativeVertical)RelativeVertical.Page;
            image.RelativeHorizontal = (MigraDocCore.DocumentObjectModel.Shapes.RelativeHorizontal)RelativeHorizontal.Page;
            image.WrapFormat.Style = (MigraDocCore.DocumentObjectModel.Shapes.WrapStyle)WrapStyle.Through;

            //two cols design
            var textFrame = section.AddTextFrame();
            var innerTable = textFrame.AddTable();
            innerTable.Borders.Visible = false;
            innerTable.AddColumn("10.5cm");
            innerTable.AddColumn("9cm"); //right indent

            //add ticket name
            var tikcetNameHeight = model.NameCm;
            var nameRow = innerTable.AddRow();
            nameRow.HeightRule = MigraDocCore.DocumentObjectModel.Tables.RowHeightRule.Exactly;
            nameRow.Height = Unit.FromCentimeter(tikcetNameHeight);
            var nameParagraph = nameRow.Cells[1].AddParagraph();
            nameParagraph.AddText(model.TicketTypeName);
            nameParagraph.AddLineBreak();

            //add white space
            var whitespaceRow = innerTable.AddRow();
            whitespaceRow.HeightRule = MigraDocCore.DocumentObjectModel.Tables.RowHeightRule.Exactly;
            whitespaceRow.Height = Unit.FromCentimeter(model.WhitespaceCm);
            var whitespace = whitespaceRow.Cells[1].AddParagraph();
            whitespace.Style = "whitespace";

            //add qr code
            //add table
            var rowWithCode = innerTable.AddRow();
            var qrTableFrame = rowWithCode.Cells[1].AddTextFrame();
            var qrTable = qrTableFrame.AddTable();
            qrTable.Borders.Visible = false;
            qrTable.AddColumn("5cm");
            qrTable.AddColumn("3cm");
            var qrRow = qrTable.AddRow();

            //add text
            var qrTextParagraph = qrRow.Cells[0].AddParagraph();
            qrTextParagraph.Style = "qrText";
            qrTextParagraph.AddText(model.QrText);
            //add image
            var qrImageParagraph = qrRow.Cells[1].AddParagraph();
            qrImageParagraph.Style = "qr";
            var qr = ImageSource.FromBinary("qr", () => model.QrImage);
            var qrPageImage = qrImageParagraph.AddImage(qr);
            qrPageImage.Width = "3cm";
            qrPageImage.Height = "3cm";

            //-------

            //Render pdf and return bytes array
            return migraManager.SavePdf(document);
        }

        /// <summary>
        /// Defines styles and styles "classes"
        /// </summary>
        /// <param name="document"></param>
        /// <param name="model"></param>
        private void DefineStyles(Document document, TicketModel model)
        {
            document.DefaultPageSetup.TopMargin = Unit.FromCentimeter(model.FromTopCm);
            document.DefaultPageSetup.BottomMargin = "0";
            document.DefaultPageSetup.LeftMargin = "0";
            document.DefaultPageSetup.RightMargin = "0";

            Style style = document.Styles["Normal"];
            style.ParagraphFormat.LeftIndent = Unit.FromCentimeter(model.ColLeftIndentCm);

            style.Font.Name = "Emtepe Regular";
            style.Font.Size = 13;
            style.Font.Bold = true;

            //name paragraph
            style = document.AddStyle("nameParagraph", "Normal");

            //whitespace paragraph
            style = document.AddStyle("whitespace", "Normal");

            //qr code paragraph
            style = document.AddStyle("qr", "Normal");

            //qr text paragraph
            float diff = 2.2f;
            style = document.AddStyle("qrText", "Normal");
            style.Font.Size = 8;
            style.ParagraphFormat.SpaceBefore = Unit.FromCentimeter(diff);
        }
    }
}
