﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentGenerator.PdfServices
{
    public class TicketModel
    {
        public byte[] BackgroundImage { get; set; } 
        public byte[] QrImage { get; set; }
        public string QrText { get; set; }
        public string TicketTypeName { get; set; }
        public float FromTopCm { get; set; }
        public float ColLeftIndentCm { get; set; }
        public float NameCm { get; set; }
        public float WhitespaceCm { get; set; }
    }
}
