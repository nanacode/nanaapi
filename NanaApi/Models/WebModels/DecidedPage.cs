﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanaApi.WebModels
{
    public class DecidedPage : WebPage
    {
        public DecidedPage(BigInfoBoxVersion version, string guid)
        {
            switch (version)
            {
                case BigInfoBoxVersion.yes:
                    SetYesVersion(guid);
                    break;
                case BigInfoBoxVersion.no:
                    SetNoVersion(guid);
                    break;
                case BigInfoBoxVersion.error:
                    SetErrorVersion(guid);
                    break;
                default:
                    BigInfoBox = null;
                    break;
            }
        }

        public BigInfoBox BigInfoBox { get; set; }

        private void SetYesVersion(string guid)
        {
            BigInfoBox = new BigInfoBox
            {
                Version = BigInfoBoxVersion.yes,
                H2Title = "My H2 title",
                H3Title = "My H3 title",
                Paragraph = $"This is a guid: <b>  {guid} </b>.",
                Alerts = new List<string>
                {
                    "<strong>Alert?</strong> Text alert."
                }
            };
        }

        private void SetNoVersion(string guid)
        {
            BigInfoBox = new BigInfoBox
            {
                Version = BigInfoBoxVersion.no,
                H2Title = "My H2 title",
                H3Title = "My H3 title",
                Paragraph = $"This is a guid: <b>  {guid} </b>.",
                Alerts = new List<string>
                {
                    "<strong>Alert?</strong> Text alert."
                }
            };
        }

        private void SetErrorVersion(string guid)
        {
            BigInfoBox = new BigInfoBox
            {
                Version = BigInfoBoxVersion.error,
                H2Title = "My H2 title",
                H3Title = "My H3 title",
                Paragraph = $"This is a guid: <b>  {guid} </b>.",
                Alerts = new List<string>
                {
                    "<strong>Alert?</strong> Text alert."
                }
            };
        }
    }
}
