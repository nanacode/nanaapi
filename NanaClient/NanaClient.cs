﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace skekWebApiClient
{
    public class NanaClient
    {
        private string _serverAddress;
        private string _apiKey;
        private bool _allowUntrustedCertificates = false;
        private HttpClient _forceOwnHttpClient = null;

        private HttpClient GetHttpClient()
        {
            if (_forceOwnHttpClient != null)
                return _forceOwnHttpClient;

            if (_allowUntrustedCertificates)
            {
                var handler = new HttpClientHandler();
                handler.ClientCertificateOptions = ClientCertificateOption.Manual;
                handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

                return new HttpClient(handler);
            }
            else
                return new HttpClient();
        }

        public NanaClient(string serverAddress, string apiKey, bool allowUntrustedCertificates = false, HttpClient forceOwnHttpClient = null)
        {
            _allowUntrustedCertificates = allowUntrustedCertificates;
            _forceOwnHttpClient = forceOwnHttpClient;
            _serverAddress = serverAddress.TrimEnd('/');
            _apiKey = apiKey;
        }


        public string PingDb()
        {
            var task = PingDbAsync();
            task.Wait();
            return task.Result;
        }

        public async Task<string> PingDbAsync()
        {
            using (var client = GetHttpClient())
            {
                client.DefaultRequestHeaders.Add("ApiKey", _apiKey);

                string url = _serverAddress + $"/nana/pingdb/";

                var httpResult = await client.GetAsync(url).ConfigureAwait(false);

                var jsonString = await httpResult.Content.ReadAsStringAsync().ConfigureAwait(false);
                //var unifiedResponse = JsonConvert.DeserializeObject<ResponseObject>(jsonString);

                return jsonString;
            }
        }


    }
}
