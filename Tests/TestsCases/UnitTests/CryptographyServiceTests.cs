using Cryptography;
using NUnit.Framework;
using System.Security.Cryptography;

namespace Tests.TestCases
{
    public class CryptographyServiceTests
    {
        private CryptographyService _service;

        [SetUp]
        public void Setup()
        {
           _service = new CryptographyService();
        }

        [TestCase("sometext", "O'ValleyofPlenty", "yQ0Qi8AMVAOAhRckIYQQIQ")]
        [TestCase("5dsadd4asd6aaasa89fa0UDASH", "O'ValleyofPlenty", "7Rvs_6LOoywiyDjYSCpcnh8B-0NXDgEh5N7aF1rEbKE")]
        public void Key128_Encrypt_ReturnEncrypted(string testText, string key, string encrypted)
        {
            var possiblyEncrypted = _service.Encrypt(testText, key);
            Assert.AreEqual(encrypted, possiblyEncrypted);
        }

        [TestCase("sometext", "O'ValleyofPlenty", "yQ0Qi8AMVAOAhRckIYQQIQ")]
        [TestCase("5dsadd4asd6aaasa89fa0UDASH", "O'ValleyofPlenty", "7Rvs_6LOoywiyDjYSCpcnh8B-0NXDgEh5N7aF1rEbKE")]
        public void Key128_Decrypt_ReturnDecrypted(string testText, string key, string encrypted)
        {
            var possiblyDecrypted = _service.Decrypt(encrypted, key);
            Assert.AreEqual(testText, possiblyDecrypted);
        }

        [TestCase("sometext", "thisis128bitskey")]
        [TestCase("sometext", "O'ValleyofPlenty")]
        public void Key128_Encrypt_DecryptReturnSame(string testText, string key)
        {
            var encrypted = _service.Encrypt(testText, key);
            Assert.AreEqual(testText, _service.Decrypt(encrypted, key));
        }

        [TestCase("sometext", "O'Valley")]
        [TestCase("sometext", "O'ValleyofPlenty O'ValleyofPlenty")]
        public void BadKey_Encrypt_ThrowsException(string testText, string key)
        {
            Assert.Throws<CryptographicException>(() => _service.Encrypt(testText, key));
        }

        [TestCase("sometext", "O'Valley")]
        [TestCase("sometext", "O'ValleyofPlenty O'ValleyofPlenty")]
        public void BadKey_Decrypt_ThrowsException(string testText, string key)
        {
            Assert.Throws<CryptographicException>(() => _service.Decrypt(testText, key));
        }

        [TestCase("sometext", "O'Valley", "pepper")]
        [TestCase("sometext sometext 23 -- sometext", "O'Valley O'Valley O'Valley", "p")]
        [TestCase("sometext sometext 23 -- sometext", "O'Vall", "salt13123 4342 434234 423 4 34 34 234 234 23")]
        public void Anykey_Anysalt_GetKeyWithSalt_Encrypt_DecryptReturnSame(string testText, string key, string salt)
        {
            var keyWithSalt = _service.Get128KeyWithSalt(key, salt);
            var encrypted = _service.Encrypt(testText, keyWithSalt);
            Assert.AreEqual(testText, _service.Decrypt(encrypted, keyWithSalt));
        }
    }
}