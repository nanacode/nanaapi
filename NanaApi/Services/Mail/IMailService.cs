﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Mail
{
    public interface IMailService
    {
        public MailMessage SendInternalMail(string addressEmailFrom, string addressEmailTo, string title, string body, List<Attachment> attachments);
    }
}
