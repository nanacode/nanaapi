﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Mail
{
    public interface ISmtpClientProvider
    {
        public SmtpClient GetSmtpClient();
    }
}
