﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Http;
using Trace;

namespace NanaApi.RequestsFilters
{
    /// <summary>
    /// Request Filter for Activity Tags logging.
    /// Adds client IP tag on request coming and dumps all collected tags to log when request is finished. 
    /// </summary>
    public class TraceLogAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var remoteIpAddress = context.HttpContext.Connection.RemoteIpAddress;

            if (remoteIpAddress != default)
            {
                TraceManager.AddRequestIpTag(remoteIpAddress.ToString());
            }
            else
            {
                TraceManager.AddRequestIpTag("unknown");
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            ILogger logger = context.HttpContext.RequestServices.GetService<ILogger<TraceLogAttribute>>();
            TraceManager.LogCurrentActivity(logger);
        }
    }
}
