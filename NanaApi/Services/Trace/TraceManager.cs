﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Trace
{
    /// <summary>
    /// Manager accessing Activity and logging tags trace
    /// </summary>
    public static class TraceManager
    {
        public const string TRACEMANAGER_POINTER = "TRACEMANAGER";
        public const string TRACEMANAGER_REQUEST_IP_TAG = "REQUEST_IP";

        public static void AddRequestIpTag(string ip)
        {
            try
            {
                Activity.Current.AddTag(TRACEMANAGER_REQUEST_IP_TAG, ip);
            }
            catch { }
        }

        public static string GetRegisteredRequestIp()
        {
            try
            {
                var activity = Activity.Current;
                var ipTag = activity.Tags.Where(a => a.Key == TRACEMANAGER_REQUEST_IP_TAG).FirstOrDefault();
                return ipTag.Value;
            }
            catch { return ""; }
        }

        /// <summary>
        /// Adds single trace tag with a file name key
        /// </summary>
        /// <param name="value">Tag info</param>
        /// <param name="memberName">DO NOT USE, this value will be auto completed during compilation time</param>
        /// <param name="filePath">DO NOT USE, this value will be auto completed during compilation time</param>
        public static void AddTraceTag(string value,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string filePath = "")
        {
            try
            {
                var fileName = Path.GetFileName(filePath);
                Activity.Current.AddTag($"{fileName}-{memberName}", value);
            }
            catch { }
        }

        /// <summary>
        /// Adds single trace tag with the custom key and value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddTraceTag(string key, string value)
        {
            try
            {
                Activity.Current.AddTag(key, value);
            }
            catch { }
        }

        /// <summary>
        /// Gets Serialized tags from the current activity
        /// </summary>
        /// <returns>string with serialized tags (and ["TRACEMANAGER_POINTER"] heading)</returns>
        public static string GetCurrentSerializedTags()
        {
            var activity = Activity.Current;
            return $"[{TRACEMANAGER_POINTER}] " + JsonConvert.SerializeObject(activity.Tags);
        }

        /// <summary>
        /// Dumps current Activity tags trace to log
        /// </summary>
        /// <param name="logger">Instance of logger</param>
        public static void LogCurrentActivity(ILogger logger)
        {
            try
            {
                logger.LogInformation(GetCurrentSerializedTags());
            }
            catch { }
        }

        /// <summary>
        /// Gets all traces with the given key
        /// </summary>
        /// <param name="key">key of traces</param>
        /// <returns>List of the trace tags with the given key</returns>
        public static List<KeyValuePair<string, string>> GetTracesByKey(string key)
        {
            try
            {
                var activity = Activity.Current;
                return activity.Tags.Where(x => x.Key == key).ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
