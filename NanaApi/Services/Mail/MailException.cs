﻿using System;
using System.Net.Mail;

namespace Mail
{
    public class MailException: Exception
    {
        public MailMessage MailMessage { get; set; }
        public string ProviderName { get; set; }

        public MailException(MailMessage mailMessage, string providerName, string message, Exception e) : base(message, e)
        {
            ProviderName = providerName;
            MailMessage = mailMessage;
        }

        public MailException(MailMessage mailMessage, ISmtpClientProvider provider, string message, Exception e) : base(message, e)
        {
            ProviderName = provider.GetType().Name;
            MailMessage = mailMessage;
        }
    }
}
