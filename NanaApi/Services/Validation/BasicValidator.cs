﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace skekModels.Validation
{
    public class Validation
    {
        /// <summary>
        /// Checks if properties fulfill DataAnnotations attributes
        /// </summary>
        /// <param name="objectWithAttr">object to check</param>
        /// <param name="code">handle code of the validation</param>
        /// <param name="msg">human-readable description of an error</param>
        /// <exception cref="ProcessValidationException">Thrown when object is invalid</exception>

        public static void ValidateAttributes(object objectWithAttr, string code, string msg)
        {
            var validationResults = new List<ValidationResult>();

            if (!Validator.TryValidateObject(objectWithAttr, new ValidationContext(objectWithAttr), validationResults))
                throw new Exception();
        }
    }
}
