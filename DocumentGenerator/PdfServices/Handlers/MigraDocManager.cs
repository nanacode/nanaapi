﻿using System.IO;
using MigraDocCore.DocumentObjectModel;
using MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes;
using MigraDocCore.Rendering;
using PdfSharpCore.Fonts;
using PdfSharpCore.Utils;
using SixLabors.ImageSharp.PixelFormats;
using MigraDoc.DocumentObjectModel.Shapes;
using System;

namespace DocumentGenerator.PdfServices.Handlers
{
    /// <summary>
    /// .NET Core Migra Doc Handler. Uses:
    /// SixLabors ImageSharp for images (to avoid OS specific graphic 
    /// interfaces like GDI),
    /// Custom ProjectFilesFontResolver for static fonts files resolving (to avoid OS fonts conflicts)
    /// </summary>
    internal class MigraDocManager
    {
        /// <summary>
        /// Prepares document
        /// </summary>
        /// <param name="title">Document title</param>
        /// <returns>Instance of MigraDoc Document</returns>
        internal Document PrepareDocument(string title)
        {
            //Implementation of ImageSource and a custom font resolver (to use custom path for fonts)
            ImageSource.ImageSourceImpl = new ImageSharpImageSource<Rgba64>();
            try
            {
                GlobalFontSettings.FontResolver = new ProjectFilesFontResolver();
            }
            catch(Exception e) { } //Global resolver is already set
            Document document = new Document();

            document.Info.Title = title;

            return document;
        }

        /// <summary>
        /// Renders MigraDoc document and converts it to bytes array
        /// </summary>
        /// <param name="document">MigraDoc document instance</param>
        /// <returns>Bytes array of rendered file</returns>
        internal byte[] SavePdf(Document document)
        {
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();

            var stream = new MemoryStream();
            pdfRenderer.PdfDocument.Save(stream);
            var contentArray = stream.ToArray();
            return contentArray;
        }
    }
}
