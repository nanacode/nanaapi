﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentGenerator
{
    /// <summary>
    /// Veery simple "QRCoder" based service for qr codes generation
    /// </summary>
    public class QrCodeService
    {
        /// <summary>
        /// Generates QR code
        /// </summary>
        /// <param name="toEncode">String to encode</param>
        /// <param name="pixels">Pixels of resulting picture</param>
        /// <returns>Bytes array of generated picture</returns>
        public byte[] GetQr(string toEncode, int pixels)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(toEncode, QRCodeGenerator.ECCLevel.Q);
            BitmapByteQRCode qrCode = new BitmapByteQRCode(qrCodeData);
            byte[] qrCodeAsBitmapByteArr = qrCode.GetGraphic(pixels);
            return qrCodeAsBitmapByteArr;
        }
    }
}