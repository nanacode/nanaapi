﻿using System.IO;
using MigraDocCore.DocumentObjectModel;
using MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes;
using MigraDocCore.Rendering;
using PdfSharpCore.Fonts;
using PdfSharpCore.Utils;
using SixLabors.ImageSharp.PixelFormats;
using MigraDoc.DocumentObjectModel.Shapes;
using DocumentGenerator.PdfServices.Handlers;
using DocumentGenerator.PdfServices;

namespace DocumentGenerator
{
    /// <summary>
    /// Service based on .NET Core MigraDoc for plain paper ticket generation
    /// </summary>
    public class BareTicketService
    {
        /// <summary>
        /// Generates ticket pdf file
        /// </summary>
        /// <param name="model">Model with ticket data</param>
        /// <returns>Bytes array of pdf file</returns>
        public byte[] GetPdf(BareTicketModel model)
        {
            //Get manager to control easily Document object
            var migraManager = new MigraDocManager();

            //Prepare document
            var document = migraManager.PrepareDocument(model.QrText);

            //Define Styles
            DefineStyles(document);

            //Draw something fancy:
            //-------
            Section section = document.AddSection();

            //add logo
            var logoParagraph = section.AddParagraph();
            logoParagraph.Format.Alignment = ParagraphAlignment.Center;
            var logo = ImageSource.FromBinary("logo", () => model.Logo);
            var logoPageImage = logoParagraph.AddImage(logo);
            logoPageImage.Width = "7cm";

            //add ticket type name
            var typeNameParagraph = section.AddParagraph();
            typeNameParagraph.Style = "typename";
            typeNameParagraph.Format.Alignment = ParagraphAlignment.Center;
            typeNameParagraph.AddText(model.TicketTypeName);

            //add qr
            var codeParagraph = section.AddParagraph();
            codeParagraph.Format.Alignment = ParagraphAlignment.Center;
            var qr = ImageSource.FromBinary("qr", () => model.QrImage);
            var qrPageImage = codeParagraph.AddImage(qr);
            qrPageImage.Width = "10cm";
            qrPageImage.Height = "10cm";

            //add ticket code
            var textParagraph = section.AddParagraph();
            textParagraph.Style = "qr";
            textParagraph.Format.Alignment = ParagraphAlignment.Center;
            textParagraph.AddText(model.QrText);

            //add process name
            var processParagraph = section.AddParagraph();
            processParagraph.Style = "process";
            processParagraph.Format.Alignment = ParagraphAlignment.Center;
            processParagraph.AddText(model.ProcessTypeName);

            //Render pdf and return bytes array
            return migraManager.SavePdf(document);
        }

        /// <summary>
        /// Defines styles and Migra Doc styles "classes"
        /// </summary>
        /// <param name="document"></param>
        /// <param name="model"></param>
        private void DefineStyles(Document document)
        {
            document.DefaultPageSetup.TopMargin = "1.5cm";
            document.DefaultPageSetup.BottomMargin = "1.5cm";
            document.DefaultPageSetup.LeftMargin = "2cm";
            document.DefaultPageSetup.RightMargin = "2cm";

            Style style = document.Styles["Normal"];
            style.Font.Name = "Emtepe Regular";
            style.Font.Size = 16;

            //ticket type name
            style = document.AddStyle("typename", "Normal");
            style.ParagraphFormat.SpaceBefore = Unit.FromCentimeter(2.5);
            style.ParagraphFormat.SpaceAfter = Unit.FromCentimeter(1.5);

            //qr code paragraph
            style = document.AddStyle("qr", "Normal");
            style.ParagraphFormat.SpaceBefore = Unit.FromCentimeter(2);

            //process paragraph
            style = document.AddStyle("process", "Normal");
            style.ParagraphFormat.SpaceBefore = Unit.FromCentimeter(2.5);
        }

    }
}
