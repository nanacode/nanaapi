﻿# [testing lib] - features
- XUnit Integration tests with examples
- Unit Tests with NUnit
- SortOrder of XUnit Tests
- In-memory-sqlite-database-from-whateverbaseyouwant-context
- mocking WHOLE Api class
- testing API client 

#### XUnit plot twists
One thing needed to be described wider is a flow of Xunit testing.

1. **Creation of a concrete WebApplicationFactory**
(WebApplicationFactoryWithInMemorySqlite) that class
will become a mock of the entire WebApi with Startup, DI and so on. 
There you can override services setup including DB (in our tests 
SQLite inmemory database is used).

2. Creation of **IClassFixture** (BaseForTests) that generic class
must use WebApplicationFactory as its type. 

3. Now technically, you can type your awesome test as a method
with [Fact] attribute. In fact, we inherit from base test to avoid
redundant configurations. 

Before you will be ready to go there are a few plot twists you want 
to hear before being too confused to carry on. 

### XUnit Integration Tests - plot twists in project
**1st Plot Twist - NanaClient property of BaseForTests returns always 
new instance of class.**

The reason is "using" in the NanaClient methods. After using scope
HttpClient is disposed and we cannot use it again. 

**2nd Plot Twist - hobbitsContext can be created on the base of MS database,
but in the tests we can are using SQLite?**

As long as you don't use complicated SQL structures typical for your 
type of db (e.g. stored procedure) everything is going to work well.
SQLite is...lite and relational so is great for the tests purposes.

**3rd Plot Twist - why there are so many test classes, why can't you
use one class and a couple of methods.**

One class, one instance of inmemory database. In our integration tests 
to simplify testing we usually want to work on the clear database.
When you will insert one row to db, you can get first one after that,
without knowing specific id. 

**4th Plot Twist - what the... [Collection("Sequential")] ?**

Unfortunately inmemory database sometimes doesn't want to allow
parallel thread in. To avoid parallel accesses we have forced tests
to run sequential by placing them in the same collection. 
*[Collection("Sequential")]* means that methods with that attribute
belongs to Collection named "Sequential" (it could be "RickAstley" or 
"WhateverYouWant" name).

**5th Plot Twist - What the... Dispose() ?**

Forget about [SetUp] and [TearDown]. Now you have an constructor
and a Dispose method. Oh, don't forget to implement IDisposable. 
Smile.

**6th Plot Twist - where will be service provider**

To access DI container you need to sweat a little (as with everything
in XUnit). Here is an easy snippet for you:

``` c#
_factory.WithWebHostBuilder(builder =>
{
    builder.ConfigureServices(services =>
    {
        var serviceProvider = services.BuildServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var scopedServices = scope.ServiceProvider;
            var context = scopedServices.GetRequiredService<SkekContext>();
        }
    });
}).CreateClient(new WebApplicationFactoryClientOptions { AllowAutoRedirect = false });
```

**Summary**
All negative emotions around XUnit are only personal opinion. It is a good
framework. Thats what the people say. If you are still lost here is somewhat
good Microsoft tutorial:

https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-3.1#introduction-to-integration-tests

