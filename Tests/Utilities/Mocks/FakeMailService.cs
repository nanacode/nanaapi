﻿using Mail;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Tests.Utilities
{
    public class FakeMailService : IMailService
    {
        public MailMessage SendInternalMail(string addressEmailFrom, string addressEmailTo, string title, string body, List<Attachment> attachments)
        {
            //this email is not going anywhere :)
            return new MailMessage();
        }
    }
}
