﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp.Html.Dom;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using skekWebApiClient;
using Xunit;

namespace Tests.Utilities
{
    public class BaseForTests : IClassFixture<WebApplicationFactoryWithInMemorySqlite<NanaApi.Startup>>
    {
        protected readonly WebApplicationFactoryWithInMemorySqlite<NanaApi.Startup> _factory;
        protected readonly string _address = "";
        protected readonly string _apiKey = "bxOQtwo5UGb8vlEoQ31NdlJyMM7qnaBU";


        protected HttpClient _client;
        protected HttpClient Client 
        {
            get 
            {
                var res = _factory.CreateClient(new WebApplicationFactoryClientOptions { AllowAutoRedirect = false });
                res.DefaultRequestHeaders.Add("ApiKey", _apiKey);
                return res;
            }
            set 
            {
                _client = value;
            }
        }
        
        protected NanaClient _nanaClient;
        protected NanaClient NanaClient
        {
            get 
            { 
                return new NanaClient(_address, _apiKey, true, _factory.CreateClient(new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                }));
            } 
            set 
            {
                _nanaClient = value;
            } 
        }
   

        public BaseForTests(WebApplicationFactoryWithInMemorySqlite<NanaApi.Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
            _nanaClient = new NanaClient(_address, _apiKey, true, _client);
        }

    }
}
