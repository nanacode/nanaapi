﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Json
{
    /// <summary>
    /// Serializes despite [Newtonsoft.Json.JsonIgnore]
    /// Resolver for Newtonsoft SerializeObject allowing to serialize whole model despite it has some JsonIgnore Attributes
    /// </summary>
    public class JsonIgnoreAttributeIgnorerContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            property.Ignored = false; // Here is the magic
            return property;
        }
    }

    //Example
    //JsonConvert.SerializeObject(someComplexObject, new JsonSerializerSettings { ContractResolver = new JsonIgnoreAttributeIgnorerContractResolver() }), 
}
