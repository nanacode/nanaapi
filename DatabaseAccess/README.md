﻿# [useful lib] for docs - features
- Database scaffolding example
- SQLite local integration (not super useful but still)
- Injecting db context example (DI) - HobbitsAccess (see injection in Startup.cs in NanaApi)


#### Scaffolding instruction - how to generate models

0. Copy here sqlite db
1. Set DatabaseAccess as startup project *(known issue - scaffold requires StartUp project to reference EntityFrameworkCore)*
2. Run Package Manager Console and type:
```
Scaffold-DbContext -Connection "Data Source=hobbits.db" -Provider Microsoft.EntityFrameworkCore.Sqlite -Force
```
3. From context file remove default method "OnConfiguring". 
```c#
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
   if (!optionsBuilder.IsConfigured)
   {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("<connection string>");
   }
}
```

Instead of this prepare a DI mechanism in a main project. 