﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using DocumentGenerator;
using DocumentGenerator.PdfServices;
using Mail;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NanaApi.RequestsFilters;
using NanaApi.WebModels;
using Newtonsoft.Json;
using Resources;
using Trace;

namespace NanaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [TraceLog]
    public class NanaController : Controller
    {
        private readonly ILogger<NanaController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _services;

        public NanaController(ILogger<NanaController> logger, IConfiguration configuration, IServiceProvider services)
        {
            _logger = logger;
            _configuration = configuration;
            _services = services;
            //If you don't want to (or if you are not able to) injects via constructor:
            //var ticketStrategy = ActivatorUtilities.CreateInstance<SkekTicketStrategy>(_services);
        }

        [HttpGet("[action]")]
        [ApiKeyAuth]
        public ActionResult PingApiKey()
        {
            return Ok("I know you, you can go in.");
        }

        [HttpGet("[action]")]
        public ActionResult PingIp()
        {
            var x = TraceManager.GetRegisteredRequestIp();
            return Ok($"Hi {x}, I'm ready to roll");
        }

        [HttpGet("[action]/{version}")]
        public ActionResult PingHtml(int version)
        {
            if (version == 1)
            {
                var x = new DecidedPage(BigInfoBoxVersion.yes, "XTESTX-202021");
                TempData["DecidedPage"] = JsonConvert.SerializeObject(x);
            }
            else if (version == 2)
            {
                var x = new DecidedPage(BigInfoBoxVersion.no, "XTESTX-202022");
                TempData["DecidedPage"] = JsonConvert.SerializeObject(x);
            }
            else if (version == 3)
            {
                var x = new DecidedPage(BigInfoBoxVersion.error, "XTESTX-202023");
                TempData["DecidedPage"] = JsonConvert.SerializeObject(x);
            }

            return RedirectToAction("Decided", "Nana");
        }

        [Route("[action]")]
        public IActionResult Decided()
        {
            if (TempData["DecidedPage"] == default)
                return StatusCode(404);

            var decidedPage = JsonConvert.DeserializeObject<DecidedPage>(TempData["DecidedPage"] as string);
            if (decidedPage != null)
            {
                decidedPage.SeoTitle = "Decyzja audytora MTP - SKEK - TRIN";
                return View(decidedPage);
            }
            else
                return StatusCode(404);
        }

        [HttpGet("[action]")]
        public ActionResult PingEnv()
        {
            var environmentName = _configuration.GetValue<string>("EnvSettingsName");
            return Ok(environmentName);
        }

        [HttpGet("[action]")]
        public ActionResult PingDb()
        {
            var dal = ActivatorUtilities.CreateInstance<DatabaseAccess.HobbitsAccess>(_services);
            return Ok(dal.GetFirstHobbit());
        }

        [HttpGet("[action]")]
        public ActionResult PingDocMail()
        {
            try
            {
                var bareTicketGenerator = new BareTicketService();
                var qrCodeGeneartor = new QrCodeService();
                var qr = qrCodeGeneartor.GetQr("ABCABC", 100);
                var bareModel = new BareTicketModel
                {
                    QrImage = qr,
                    QrText = "ABCABC",
                    Logo = new ResourcesReader().GetSomePng(),
                    ProcessTypeName = "Some text",
                    TicketTypeName = "Some tex2"
                };
                var bytes = bareTicketGenerator.GetPdf(bareModel);
                var mail = _services.GetService<IMailService>();
                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(new MemoryStream(bytes), "biletmarzen.pdf");
                var list = new List<System.Net.Mail.Attachment>();
                list.Add(att);

                mail.SendInternalMail(
                    "ewarudol@protonmail.com",
                    "ewarudol@protonmail.com",
                    "Mail Ping", 
                    new ResourcesReader().GetSomeText(), list);

                return Ok("Ping mail sent to ewarudol@protonmail.com");
            }
            catch (Exception e)
            {
                _logger.LogCritical("MailPing not working!", e);
                return StatusCode(500, "Something went wrong, see log.");
            }
        }
    }
}
