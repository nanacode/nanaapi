﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Mail
{
    /// <summary>
    /// SmtpClient provider for testing, debugging and development purposes
    /// </summary>
    public class DummySmtpClientProvider : ISmtpClientProvider
    {
        public SmtpClient GetSmtpClient()
        {
            return new SmtpClient
            {
                Host = "localhost",
                Port = 25,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
        }
    }
}
