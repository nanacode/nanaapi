﻿using System;
using System.Linq;
using DatabaseAccess;
using Mail;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NUnit.Framework.Constraints;

namespace Tests.Utilities
{
    public class WebApplicationFactoryWithInMemorySqlite<TStartup>: WebApplicationFactory<TStartup> where TStartup : class
    {
        private readonly string _connectionString = "Filename=:memory:";
        private readonly SqliteConnection _connection;

        public WebApplicationFactoryWithInMemorySqlite()
        {
            _connection = new SqliteConnection(_connectionString);
            _connection.Open();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder) =>
            builder.ConfigureServices(services =>
            {
                //remove old db
                var dbDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<hobbitsContext>));
                services.Remove(dbDescriptor);

                //Overrides
                //Mock db (SQLite in memory db)
                services
                    .AddEntityFrameworkSqlite()
                    .AddDbContext<hobbitsContext>(options =>
                    {
                        options.UseSqlite(_connection);
                    });

                //Mail service
                var mailDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(IMailService));
                services.Remove(mailDescriptor);
                services.AddScoped<IMailService, FakeMailService>();

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<hobbitsContext>();
                    var logger = scopedServices.GetRequiredService<ILogger<WebApplicationFactoryWithInMemorySqlite<TStartup>>>();
                    db.Database.EnsureCreated();

                    try
                    {
                        db.Hobbits.Add(new Hobbits { Name = "Sam" });
                        db.SaveChanges();
                        //prepare initial db data if you need to
                        //for example: db.Ticket.Add(new Ticket());
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "An error occurred seeding the " +
                            "database with test messages. Error: {Message}", ex.Message);
                    }
                }
            });

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _connection.Close();
        }
    }

}
